#!/usr/bin/env python3

"""
Inpher (https://www.inpher.io/)
| New York (USA), Lausanne (Switzerland), Paris (France)
| Software Engineers | Full-time | Onsite

At Inpher, we believe that privacy and security are foundational to the
future of computing and have built enterprise products to make this vision
a reality. We are a small team of veteran founders, world-renowned
cryptographers and proven software engineers. We are headquartered in
New York City, with satellite offices in San Francisco and Lausanne,
Switzerland, and have raised $14M in funding.

Apply at https://www.inpher.io/careers

Or email to me directly at $user@inpher.io, where each ascii character
of $user can be obtained by solving for x and converting to base-128,

x = 145767 mod 611939, x = 109572 mod 598463 
"""

#x1 = 145767 % 611939
#x2 = 109572 % 598463

b = 611939
q = 598463

# b * t === 562268 (mod q)
bt = 562268

for i in range(1, 20):
    print((b*i) % q)

