Hopper (US, Canada, East Coast, "100% remote" - ale chyba jednak nie)
https://news.ycombinator.com/item?id=24651639&ref=hvper.com
szukaj na stronie: hopper

- Nie wiadomo czego chcą, ale zatrudniają na potęgę i dopiero się rozkręcają. "100% remote"
- Robią usługę dla planowania podróży; oparte na mikroservicach; wyglądają na sfinansowanych i z pomysłem; dobre wrażenie

---

Nightwatch (remote only)
https://nightwatch.io/jobs/backend-developer

- Budują SEO
- All the right words… wszystko wygląda tak pięknie, że aż za pięknie
- Ciekawe podejście do rekrutacji (upload you desktop)
- Mix technologii (w tym Rust)
- Proszą o podanie ile chce się kasy
- Mały team, ale wygląda ok

---

SUSE (?) Prague, Nuremberg
https://jobs.suse.com/us/en/job/7011559/Automation-Engineer-Infrastructure

---

Radix Labs - Bingo pod każdym względem
https://radix.bio/how-it-works
interview to chyba najpierw zadanie z kodem

Radix Labs | Software Engineers esp. Front End Engineers | Cambridge, MA, USA (Boston area) | Full Time and Co-Op/INTERNS Biology has talented PhD Biologists spend 5 hours a day on hard to reproduce, unoptimized minutia that isn’t innovative. Radix Labs hopes to accelerate biotech innovation like early compilers did for computing by enabling lab protocols to seamlessly operate and be optimized across physical labs. Our software makes biology reproducible, faster, more efficient, and lets smart biologists focus on the science instead of managing the implementation. We do this by developing a compiler, operating system, scheduler, and device drivers for biology labs and protocols. We have funding from Ycombinator (S18), Firstminute Capital, MIT's The Engine, and Nikesh Arora.
Technologies: 100% Scala code base but we don't require you know it already. (It's helpful if you have past functional programming experience with Erlang, Ocaml, Prolog, Haskell, or similar.) Other tech: HashiCorp stack (Nomad, Consul, & Terraform), Apache Kafka, Akka, ScalaJS React, Z3, Cats, Bazel w/ rules_scala, (some) AWS.
We're especially looking for an experienced front end developer to lead developing our front end visual programming language that biologists use to describe their protocols to the rest of our software. Come join our world-class diverse, inclusive, and caring team! You won't be the only woman, trans person, POC, latino, non-US citizen, wearer of silly hats, or parakeet! We will consider remote candidates and those who need visa sponsorship on a case-by-case basis. We offer equity, great salary, unlimited PTO, medical, dental, and vision insurance, mechanical keyboard budget, and flexible work hours. Any questions, send me an email: halie at radix.bio
FT app and job descriptions: https://forms.gle/1d3qE466kVFir8pf9
Co-Op app and job descriptions: https://forms.gle/6qY9Gx4dkVtHSFXf8
Technical business development, sales, and marketing (biology, manufacturing/operations, or CS competent): https://forms.gle/WWo9A43TWagR2Pp8A

---

Inpher (https://www.inpher.io/) | New York (USA), Lausanne (Switzerland), Paris (France) | Software Engineers | Full-time | Onsite

At Inpher, we believe that privacy and security are foundational to the
future of computing and have built enterprise products to make this vision
a reality. We are a small team of veteran founders, world-renowned
cryptographers and proven software engineers. We are headquartered in
New York City, with satellite offices in San Francisco and Lausanne,
Switzerland, and have raised $14M in funding.

Apply at https://www.inpher.io/careers

Or email to me directly at $user@inpher.io, where each ascii character of $user can be obtained by solving for x and converting to base-128,

x = 145767 mod 611939, x = 109572 mod 598463

----
https://immunant.com/
guys off Q3->Rust
remote for US only?


---

Onai | | San Jose / New York | FULL TIME, CONTRACTORS, GRADUATE INTERNS, POSTDOCTORAL FELLOWS, REMOTE

We're tackling exciting difficult challenges and building offerings relevant to interesting real-world problems in a variety of fields. We have particular strengths in dispersed computation, functional programming, cryptography, and deep learning.

We're currently most interested in engineers with solid experience in Rust, Haskell/Idris, or cryptography. We also have openings for enthusiastic developers or researchers who might lack this precise experience but are eager and able to learn. We welcome internship/fellowship interest from postdoctoral scholars or senior graduate students.

We do not presently have openings for current/recent undergraduates.

Wherever you are in the world, send your resume to info@onai.com and we'll let you know if there's a potential fit. 


---
https://apply.workable.com/aiven/j/9067F81350/
